import java.security.MessageDigest

class BaseTestJob{
    def jenkins
    void run(){
        jenkins.node{
            List envVars = []
            List paths = []
            String javaHome = jenkins.tool "jdk-17.0.5"
            envVars << "JAVA_HOME=${javaHome}"
            paths << "${javaHome}/bin"
            String sonarScannerHome = jenkins.tool "SonarScanner"
            envVars << "SONAR_SCANNER_BIN=${sonarScannerHome}/bin/sonar-scanner"
            if(paths){
                envVars << "PATH+TOOLS=${paths.join(':')}"
            }
            jenkins.withEnv(envVars){
                jenkins.echo "${envVars}"
            }
        }

        // test
        // jenkins.withEnv([]){
        //     jenkins.stage("prepare") {
        //         jenkins.echo "node success"
        //     }
        // }
    }

    def getGitParamsFromGitlabTrigger(String credentialId){
        if(!jenkins.env.gitlabSourceBranch) return null
        [
            url: jenkins.env.gitlabSourceRepoSshURL
        ]
    }

}

new BaseTestJob(jenkins: this).run()